all: bin/listas test bin/dados.txt

clean:
	rm bin/*

bin/listas: main.cpp
	g++ -std=c++11 -Wall -O2 main.cpp -o bin/listas

bin/test: test.cpp
	g++ -std=c++11 -Wall -g -O0 test.cpp -o bin/test

bin/dados.txt: gerador_dados.py
	python gerador_dados.py 50000000 > bin/dados.txt

test: bin/test
	bin/test

experimento: bin/dados.txt bin/listas
	bin/listas < bin/dados.txt
