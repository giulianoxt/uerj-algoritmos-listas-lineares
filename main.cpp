#include <iostream>
#include <algorithm>
#include <fstream>
#include <cstdlib>

#include "aluno.h"
#include "sequencial.h"
#include "encadeada.h"
#include "cronometro.h"

using namespace std;

vector<Aluno> dados;


template<typename Lista>
void experimento_insercao_remocao_lista(ostream& out, Lista& l, uint n_max, uint num_pontos) {
	auto n_start = 100;
	auto step = max((n_max - n_start) / num_pontos, 1u);

	for (uint n = n_start; n <= n_max; n += step) {

		auto t_insercao = cronometrar([&] {
			for (uint i = 0; i < n; ++i)
				Insere(l, dados[i]);
		});

		auto t_remocao = cronometrar([&] {
			for (uint i = 0; i < n; ++i)
				Remove(l, dados[i]);
		});

		out << n << ", " << ms(t_insercao) << ", " << ms(t_remocao) << endl;
	}
}


template<typename Lista>
void experimento_busca_lista(ostream& out, Lista& l, uint n_max, uint num_pontos, uint num_repeticoes) {
	auto n_start = 100;
	auto step = max((n_max - n_start) / num_pontos, 1u);

	for (uint n = n_start; n <= n_max; n += step) {
		out << n << ", " << flush;

		for (uint i = l.N; i < n; ++i)
			Insere(l, dados[i]);

		auto t_busca = cronometrar([&] {
			for (uint i = 0; i < num_repeticoes; ++i) {
				Aluno& elemento = dados[rand() % n];
				Busca(l, elemento);
			}
		}) / num_repeticoes;

		out << ms(t_busca) << endl;
	}
}


int main(void) {

	uint total;
	scanf("%u ", &total);

	cout << "Reading..." << endl;

	dados.resize(total);
	for (uint i = 0; i < total; ++i)
		scanf("%u %s %u ", &(dados[i].matricula), dados[i].nome, &(dados[i].idade));		

	lista_sequencial<Aluno> sequencial(total);
	lista_encadeada<Aluno> encadeada;

	//////////////////

	cout << "sequencial - exp1" << endl;
	ofstream seq_exp1("out/seq_exp1.csv");

	seq_exp1 << "sequencial, t_insercao_ms, t_remocao_ms" << endl;
	random_shuffle(dados.begin(), dados.end());
	experimento_insercao_remocao_lista(seq_exp1, sequencial, 71000, 100);

	//////////////////

	cout << "sequencial - exp2" << endl;
	ofstream seq_exp2("out/seq_exp2.csv");

	seq_exp2 << "sequencial, t_busca_ms" << endl;
	sort(dados.begin(), dados.end());
	experimento_busca_lista(seq_exp2, sequencial, 50000000, 100, 1000000);

	//////////////////

	cout << "encadeada - exp1" << endl;
	ofstream enc_exp1("out/enc_exp1.csv");

	enc_exp1 << "encadeada, t_insercao_ms, t_remocao_ms" << endl;
	random_shuffle(dados.begin(), dados.end());
	experimento_insercao_remocao_lista(enc_exp1, encadeada, 41000, 100);

	//////////////////

	cout << "encadeada - exp2" << endl;
	ofstream enc_exp2("out/enc_exp2.csv");

	enc_exp2 << "encadeada, t_busca_ms" << endl;
	sort(dados.rbegin(), dados.rend());
	experimento_busca_lista(enc_exp2, encadeada, 30000000, 100, 100);

	return 0;
};
