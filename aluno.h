#ifndef ALUNO_H
#define ALUNO_H

struct Aluno {
	uint matricula;
	char nome[30];
	uint idade;

	bool operator==(const Aluno& a) const {
		return matricula == a.matricula;
	}

	bool operator<(const Aluno& a) const {
		return matricula < a.matricula;
	}
};

#endif
