import sys
from string import ascii_letters
from random import seed, choice


if __name__ == '__main__':
	seed(0)

	N = int(sys.argv[1])

	print N
	for i in xrange(N):
		print i, ascii_letters[i % len(ascii_letters)] * 5, (i % 30) + 5
