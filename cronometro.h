#ifndef TIME_IT_H
#define TIME_IT_H

#include <chrono>
using namespace std::chrono;


template<typename Func>
double cronometrar(Func f) {
	auto t1 = high_resolution_clock::now();
	f();
	auto t2 = high_resolution_clock::now();

	return duration_cast<nanoseconds>(t2 - t1).count();
}


double ms(double nanoseconds) {
	return nanoseconds * 1.0e-6;
}


#endif
