#ifndef SEQUENCIAL_H
#define SEQUENCIAL_H


template<typename T>
struct lista_sequencial {
	T* elementos;
	uint N;
	uint capacidade;

	lista_sequencial(uint cap) {
		elementos = new T[cap];
		capacidade = cap;
		N = 0;
	}

	~lista_sequencial() {
		delete[] elementos;
	}
};


template<typename T>
T AcessaPosicao(lista_sequencial<T>& l, uint i) {
	if (i >= l.N)
		throw "Out of bounds";

	return l.elementos[i];
}


template<typename T>
int Busca(lista_sequencial<T>& l, const T& x) {
	int inf = 0, sup = l.N-1;

	while (inf <= sup) {
		int mid = (inf + sup) / 2;

		if (l.elementos[mid] == x)
			return mid;
		else if (l.elementos[mid] < x)
			inf = mid + 1;
		else
			sup = mid - 1;
	}

	return -(inf + 1);
}


template<typename T>
void InserePosicao(lista_sequencial<T>& l, const T& x, uint i) {
	if (l.N == l.capacidade)
		throw "Overflow";

	for (uint j = l.N; j > i; --j)
		l.elementos[j] = l.elementos[j-1];
	
	l.elementos[i] = x;
	l.N += 1;
}


template<typename T>
void Insere(lista_sequencial<T>& l, const T& x) {
	int res = Busca(l, x);
	if (res >= 0)
		throw "Elemento existente";

	uint pos = -(res + 1);
	InserePosicao(l, x, pos);
}


template<typename T>
void RemovePosicao(lista_sequencial<T>& l, uint i) {
	if (l.N == 0)
		throw "Underflow";

	for (uint j = i; j < l.N-1; ++j)
		l.elementos[j] = l.elementos[j+1];

	l.N -= 1;
}


template<typename T>
void Remove(lista_sequencial<T>& l, const T& x) {
	int pos = Busca(l, x);
	if (pos < 0)
		throw "Elemento inexistente";

	RemovePosicao(l, pos);
}


#endif
