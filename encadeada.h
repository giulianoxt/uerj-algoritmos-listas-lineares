#ifndef ENCADEADA_H
#define ENCADEADA_H


template<typename T>
struct No {
	T elemento;
	No<T> *prev = nullptr;
	No<T> *next = nullptr;
};


template<typename T>
struct lista_encadeada {
	No<T>* head;
	uint N;

	lista_encadeada() {
		head = new No<T>();
		N = 0;
	}

	~lista_encadeada() {
		auto it = head;

		while (it) {
			auto next = it->next;
			delete it;
			it = next;
		}
	}
};


template<typename T>
T AcessaPosicao(lista_encadeada<T>& l, uint i) {
	if (i >= l.N)
		throw "Out of bounds";

	auto no = l.head->next;
	for (; i; --i)
		no = no->next;

	return no->elemento;
}


template<typename T>
No<T>* Busca(lista_encadeada<T>& l, const T& x, No<T>** no_anterior = nullptr) {
	No<T>* tmp;
	if (!no_anterior)
		no_anterior = &tmp;
	
	*no_anterior = l.head;

	for (auto no = l.head->next; no; *no_anterior = no, no = no->next)
		if (no->elemento == x)
			return no;
		else if (x < no->elemento)
			return nullptr;

	return nullptr;
}


template<typename T>
void Insere(lista_encadeada<T>& l, const T& x) {
	No<T>* no_anterior;

	if (Busca(l, x, &no_anterior))
		throw "Elemento existente";

	auto no = new No<T>();
	no->elemento = x;
	no->prev = no_anterior;
	no->next = no_anterior->next;

	no->prev->next = no;
	if (no->next)
		no->next->prev = no;

	l.N += 1;
}


template<typename T>
void Remove(lista_encadeada<T>& l, const T& x) {
	auto no = Busca(l, x);
	if (!no)
		throw "Elemento inexistente";

	no->prev->next = no->next;
	if (no->next)
		no->next->prev = no->prev;

	delete no;
	l.N -= 1;
}

#endif
