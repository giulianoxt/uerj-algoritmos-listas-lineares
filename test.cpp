#include <iostream>
#include "sequencial.h"
#include "encadeada.h"

using namespace std;

#define assert_(exp) \
	if (!(exp)) { cerr << "Failed! Assertion failed at line " << __LINE__ << endl; exit(1); }


void testCriaNovaListaSequencial() {
	lista_sequencial<int> l(40);

	assert_(l.N == 0);
	assert_(l.capacidade == 40);
}


void testInsercaoListaSequencial() {
	lista_sequencial<int> l(10);

	Insere(l, 10);
	assert_(l.N == 1);
	assert_(AcessaPosicao(l, 0) == 10);

	Insere(l, 20);
	assert_(AcessaPosicao(l, 1) == 20);

	Insere(l, 3);
	Insere(l, 5);
	Insere(l, 12);
	Insere(l, 11);
	Insere(l, 0);
	Insere(l, 30);

	assert_(l.N == 8);
	assert_(AcessaPosicao(l, 0) == 0);
	assert_(AcessaPosicao(l, 1) == 3);
	assert_(AcessaPosicao(l, 2) == 5);
	assert_(AcessaPosicao(l, 3) == 10);
	assert_(AcessaPosicao(l, 4) == 11);
	assert_(AcessaPosicao(l, 5) == 12);
	assert_(AcessaPosicao(l, 6) == 20);
	assert_(AcessaPosicao(l, 7) == 30);
}


void testBuscaListaSequencial() {
	lista_sequencial<int> l(20);
	for (int i = 0; i < 10; ++i)
		Insere(l, i);

	Insere(l, 500);

	assert_(Busca(l, 0) == 0);
	assert_(Busca(l, 3) == 3);
	assert_(Busca(l, 500) == 10);
	assert_(Busca(l, 100) == -(10 + 1));
	assert_(Busca(l, -10) == -(0 + 1));
}


void testRemocaoListaSequencial() {
	lista_sequencial<int> l(20);
	for (int i = 0; i < 10; ++i)
		Insere(l, i);

	Remove(l, 5);
	Remove(l, 0);
	Remove(l, 9);

	assert_(l.N == 7);
	assert_(Busca(l, 5) < 0);
	assert_(Busca(l, 0) < 0);
	assert_(Busca(l, 9) < 0);

	assert_(AcessaPosicao(l, 0) == 1);
	assert_(AcessaPosicao(l, 4) == 6);
	assert_(AcessaPosicao(l, 6) == 8);
}


void testCriaNovaListaEncadeada() {
	lista_encadeada<int> l;
	assert_(l.N == 0);
	assert_(Busca(l, 3) == nullptr);
}


void testInsercaoListaEncadeada() {
	lista_encadeada<int> l;

	Insere(l, 10);
	assert_(l.N == 1);
	assert_(AcessaPosicao(l, 0) == 10);

	Insere(l, 20);
	assert_(AcessaPosicao(l, 1) == 20);

	Insere(l, 3);
	Insere(l, 5);
	Insere(l, 12);
	Insere(l, 11);
	Insere(l, 0);
	Insere(l, 30);

	assert_(l.N == 8);
	assert_(AcessaPosicao(l, 0) == 0);
	assert_(AcessaPosicao(l, 1) == 3);
	assert_(AcessaPosicao(l, 2) == 5);
	assert_(AcessaPosicao(l, 3) == 10);
	assert_(AcessaPosicao(l, 4) == 11);
	assert_(AcessaPosicao(l, 5) == 12);
	assert_(AcessaPosicao(l, 6) == 20);
	assert_(AcessaPosicao(l, 7) == 30);
}


void testBuscaListaEncadeada() {
	lista_encadeada<int> l;
	for (int i = 0; i < 10; ++i)
		Insere(l, i);

	Insere(l, 500);

	assert_(Busca(l, 0)->elemento == 0);
	assert_(Busca(l, 3)->elemento == 3);
	assert_(Busca(l, 500)->elemento == 500);
	assert_(Busca(l, 100) == nullptr);
	assert_(Busca(l, -10) == nullptr);
}


void testRemocaoListaEncadeada() {
	lista_encadeada<int> l;
	for (int i = 0; i < 10; ++i)
		Insere(l, i);

	Remove(l, 5);
	Remove(l, 0);
	Remove(l, 9);

	assert_(l.N == 7);
	assert_(Busca(l, 5) == nullptr);
	assert_(Busca(l, 0) == nullptr);
	assert_(Busca(l, 9) == nullptr);

	assert_(AcessaPosicao(l, 0) == 1);
	assert_(AcessaPosicao(l, 4) == 6);
	assert_(AcessaPosicao(l, 6) == 8);
}


int main(void) {
	cout << "Rodando testes..." << endl;

	testCriaNovaListaSequencial();
	testInsercaoListaSequencial();
	testBuscaListaSequencial();
	testRemocaoListaSequencial();

	testCriaNovaListaEncadeada();
	testInsercaoListaEncadeada();
	testBuscaListaEncadeada();
	testRemocaoListaEncadeada();

	cout << "Testes OK!" << endl;
	return 0;
}
